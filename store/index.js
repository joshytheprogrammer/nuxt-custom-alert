export const state = () => ({
  alert: {
    isShowing: false,
    type: '',
    summary: '',
    message: ''
  }
})

export const mutations = {
  TRIGGER_ALERT(state, payload) {
    state.alert.isShowing = true
    state.alert.type = payload.type
    state.alert.summary = payload.summary
    state.alert.message = payload.message
  },
  CLOSE_ALERT(state) {
    state.alert.isShowing = false
  }
}

export const actions = {
  triggerAlert({commit}, alert) {
    commit("TRIGGER_ALERT", alert)
  },
  closeAlert({commit}) {
    commit("CLOSE_ALERT")
  }
}

export const getters = {
  isAlertShowing(state) {
    return state.alert.isShowing
  },
  returnAlert(state) {
    return state.alert
  }
}